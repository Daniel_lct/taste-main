
$(document).ready(function(){
    var swiper = new Swiper('.swiper-container', {
        spaceBetween: 30,
        effect: 'fade',
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });

    var swiperAdvantage = new Swiper('.advantage__slider', {
        // effect: 'fade',
        slidesPerColumn: 1,
        centeredSlides: false,
        spaceBetween: 40,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });

    $(document).ready(function(){
        if($(window).width() >= 1300){
            swiperAdvantage.destroy()
            swiperAdvice.destroy()
            swiperCompare.destroy()
        }
    })

    var swiperAdvice = new Swiper('.advice__slider', {
        // effect: 'fade',
        slidesPerColumn: 1,
        centeredSlides: true,
        spaceBetween: 40,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });

    var swiperCompare = new Swiper('.compare__slider', {
        // effect: 'fade',
        slidesPerColumn: 1,
        centeredSlides: true,
        spaceBetween: 0,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });

    var swiperCatalogNav1 = new Swiper('.catalog__nav-1', {
        effect: 'coverflow',
        slidesPerColumn: 1,
        spaceBetween: 0,
        slidesPerView: 'auto',
        grabCursor: true,
        centeredSlides: true,
        initialSlide: 1,
        coverflowEffect: {
            rotate: 100,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: false,
        },
        breakpoints: {
            768: {
                spaceBetween: 50,
                slidesPerView: '4',
                coverflowEffect: {
                    rotate: 10,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: false,
                },
            }
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            type: 'fraction',
        },
    });

    var swiperCatalogNav2 = new Swiper('.catalog__nav-2', {
        effect: 'coverflow',
        slidesPerColumn: 1,
        spaceBetween: 0,
        slidesPerView: 'auto',
        grabCursor: true,
        centeredSlides: true,
        initialSlide: 1,
        coverflowEffect: {
            rotate: 100,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: false,
        },
        breakpoints: {
            768: {
                spaceBetween: 50,
                slidesPerView: '4',
                coverflowEffect: {
                    rotate: 10,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: false,
                },
            }
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            type: 'fraction',
        },
    });

    var swiperCatalogNav3 = new Swiper('.catalog__nav-3', {
        effect: 'coverflow',
        slidesPerColumn: 1,
        spaceBetween: 0,
        slidesPerView: 'auto',
        grabCursor: true,
        centeredSlides: true,
        initialSlide: 1,
        coverflowEffect: {
            rotate: 100,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: false,
        },
        breakpoints: {
            768: {
                spaceBetween: 50,
                slidesPerView: '4',
                coverflowEffect: {
                    rotate: 10,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: false,
                },
            }
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            type: 'fraction',
        },
    });

    var swiperCatalogNav4 = new Swiper('.catalog__nav-4', {
        effect: 'coverflow',
        slidesPerColumn: 1,
        spaceBetween: 0,
        slidesPerView: 'auto',
        grabCursor: true,
        centeredSlides: true,
        initialSlide: 1,
        coverflowEffect: {
            rotate: 100,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: false,
        },
        breakpoints: {
            768: {
                spaceBetween: 50,
                slidesPerView: '4',
                coverflowEffect: {
                    rotate: 10,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: false,
                },
            }
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            type: 'fraction',
        },
    });

    var swiperCatalogNav5 = new Swiper('.catalog__nav-5', {
        effect: 'coverflow',
        slidesPerColumn: 1,
        spaceBetween: 0,
        slidesPerView: 'auto',
        grabCursor: true,
        centeredSlides: true,
        initialSlide: 1,
        coverflowEffect: {
            rotate: 100,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: false,
        },
        breakpoints: {
            768: {
                spaceBetween: 50,
                slidesPerView: '4',
                coverflowEffect: {
                    rotate: 10,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: false,
                },
            }
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            type: 'fraction',
        },
    });



    $('.tabs__heading').on('click', function(){
        if($(this).has('is-active')){
            setTimeout(function(){
                swiperCatalogNav1.update()
                swiperCatalogNav2.update()
                swiperCatalogNav3.update()
                swiperCatalogNav4.update()
                swiperCatalogNav5.update()
            },10)
        }
    })

    var swiperChoose = new Swiper('.choose__slider', {
        // effect: 'fade',
        slidesPerColumn: 1,
        centeredSlides: true,
        spaceBetween: 0,
        allowTouchMove: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet: function (index, className) {
                if(window.innerWidth >= 1024){
                    return '<span class="' + className + ' step_' + (index + 1) +'">' + 'Шаг ' + (index + 1) + '</span>';
                } else {
                    return '<span class="' + className + '">' + (index + 1) + '</span>';
                }
            },
        },
    });



    if($('.swiper-pagination-bullet.step_1').hasClass('swiper-pagination-bullet-active')){
        $('.choose').css({'background': 'url("../img/choose-step-1-desktop.png") center no-repeat', 'background-size':'cover'})
    }

    $('.swiper-pagination-bullet.step_1').on('click dragend', function(){
        $('.choose').css({'background': 'url("../img/choose-step-1-desktop.png") center no-repeat', 'background-size':'cover'})

    })

    $('.swiper-pagination-bullet.step_2').on('click dragend', function(){
        $('.choose').css({'background': 'url("../img/choose-step-2-desktop.png") center no-repeat', 'background-size':'cover'})

    })

    $('.swiper-pagination-bullet.step_3').on('click dragend', function(){
        $('.choose').css({'background': 'url("../img/choose-step-3-desktop.png") center no-repeat', 'background-size':'cover'})
    })

    $('.swiper-pagination-bullet.step_4').on('click dragend', function(){
        $('.choose').css({'background': 'url("../img/choose-step-4-desktop.png") center no-repeat', 'background-size':'cover'})
    })

    $('.swiper-pagination-bullet.step_5').on('click dragend', function(){
        $('.choose').css({'background': 'url("../img/choose-step-5-desktop.png") center no-repeat', 'background-size':'cover'})
    })


    var swiperFeedbacks = new Swiper('.feedbacks__slider', {
        slidesPerView: 'auto',
        centeredSlides: false,
        spaceBetween: 0,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet: function (index, className) {
                return '<span class="' + className + '">' + (index + 1) + '</span>';
            },
            type: 'fraction',
        },
    });


    /**
     * Tabs in catalog section
     */
    document.querySelectorAll(".tabs").forEach((tab) => {
        // Selecting headings and blocks with content
        const tabHeading = tab.querySelectorAll(".tabs__heading");
        const tabContent = tab.querySelectorAll(".tabs__content");

        // A variable for the data attribute
        let tabName;

        // For each tab heading...
        tabHeading.forEach((element) => {
            // ...add event listener
            element.addEventListener("click", () => {
                // Disabling each tab
                tabHeading.forEach((item) => {
                    item.classList.remove("is-active");
                });

                // Enabling a tab
                element.classList.add("is-active");

                // Getting value from the data attribute
                tabName = element.getAttribute("data-tab-index");

                // Checking all the blocks with content
                tabContent.forEach((item) => {
                    // If the item has the same class as the value of the data attribute...
                    item.classList.contains(tabName)
                        ? item.classList.add("is-active")
                        : item.classList.remove("is-active");

                    // Add class 'is-active' to this item
                    // Otherwise, remove the class
                });
            });
        });
    });


    $('select').each(function(){
        var $this = $(this), numberOfOptions = $(this).children('option').length;

        $this.addClass('select-hidden');
        $this.wrap('<div class="select"></div>');
        $this.after('<div class="select-styled"></div>');

        var $styledSelect = $this.next('div.select-styled');
        $styledSelect.text($this.children('option').eq(0).text());

        var $list = $('<ul />', {
            'class': 'select-options'
        }).insertAfter($styledSelect);

        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }

        var $listItems = $list.children('li');

        $styledSelect.click(function(e) {
            e.stopPropagation();
            $('div.select-styled.active').not(this).each(function(){
                $(this).removeClass('active').next('ul.select-options').hide();
            });
            $(this).toggleClass('active').next('ul.select-options').toggleClass('select_active');
        });

        $listItems.click(function(e) {
            e.stopPropagation();
            $styledSelect.text($(this).text()).removeClass('active');
            $this.val($(this).attr('rel'));
            $list.hide();
            //console.log($this.val());
        });

        $(document).click(function() {
            $styledSelect.removeClass('active');
            $list.hide();
        });

    });

    $(document).ready(function(){
        if($(window).width()>=1300){

            var controller = new ScrollMagic.Controller();

            let distance = '-=2'
            let offset = 2.15

            let timeline = new TimelineMax()
            timeline
                .from('.advice__cart-11', offset, {
                    // y: -400,
                    x: -2950,
                    ease: Power1.easeInOut
                }, distance)
                .from('.advice__cart-10', offset, {
                    x: -2800,
                    ease: Power1.easeInOut
                }, distance)
                .from('.advice__cart-9', offset, {
                    x: -2650,
                    ease: Power1.easeInOut
                }, distance)
                .from('.advice__cart-8', offset, {
                    x: -2500,
                    ease: Power1.easeInOut
                }, distance)
                .from('.advice__cart-7', offset, {
                    x: -2350,
                    ease: Power1.easeInOut
                }, distance)
                .from('.advice__cart-6', offset, {
                    x: -2200,
                    ease: Power1.easeInOut
                }, distance)
                .from('.advice__cart-5', offset, {
                    x: -2050,
                    ease: Power1.easeInOut
                }, distance)
                .from('.advice__cart-4', offset, {
                    x: -1900,
                    ease: Power1.easeInOut
                }, distance)
                .from('.advice__cart-3', offset, {
                    x: -1750,
                    ease: Power1.easeInOut
                }, distance)
                .from('.advice__cart-2', offset, {
                    x: -1600,
                    ease: Power1.easeInOut
                }, distance)
                .from('.advice__cart-1', offset, {
                    x: -1450,
                    ease: Power1.easeInOut
                }, distance)
                .to('.advice__slide-taste', 4, {
                    opacity: 1,
                    ease: Power1.easeInOut
                }, 1).call(function(){
                $('.advice__slide-taste').toggleClass('advice__slide-taste_hide')
            })
                .to('.advice__slide-diff', 4, {
                    opacity: 1,
                    delay: 1,
                    ease: Power1.easeInOut
                }, 3)


            new ScrollMagic.Scene({
                triggerElement: '#advice',
                duration: '300%',
                triggerHook: 0,
                offset: 0
            })
                .setTween(timeline)
                .setPin('#advice')
                .addTo(controller)
            // .addIndicators()

        }

    })


    /**
     * Compare images
     */
    $(document).ready(function(){
        $('.ba-slider').each(function(){
            var cur = $(this);
            // Adjust the slider
            var width = cur.width()+'px';
            cur.find('.resize img').css('width', width);
            // Bind dragging events
            drags(cur.find('.handle'), cur.find('.resize'), cur);
        });
    });

    // Update sliders on resize.
    // Because we all do this: i.imgur.com/YkbaV.gif
    $(window).resize(function(){
        $('.ba-slider').each(function(){
            var cur = $(this);
            var width = cur.width()+'px';
            cur.find('.resize img').css('width', width);
        });
    });

    function drags(dragElement, resizeElement, container) {

        // Initialize the dragging event on mousedown.
        dragElement.on('mousedown touchstart', function(e) {

            dragElement.addClass('draggable');
            resizeElement.addClass('resizable');

            // Check if it's a mouse or touch event and pass along the correct value
            var startX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

            // Get the initial position
            var dragWidth = dragElement.outerWidth(),
                posX = dragElement.offset().left + dragWidth - startX,
                containerOffset = container.offset().left,
                containerWidth = container.outerWidth();

            // Set limits
            minLeft = containerOffset + 10;
            maxLeft = containerOffset + containerWidth - dragWidth - 10;

            // Calculate the dragging distance on mousemove.
            dragElement.parents().on("mousemove touchmove", function(e) {

                // Check if it's a mouse or touch event and pass along the correct value
                var moveX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

                leftValue = moveX + posX - dragWidth;

                // Prevent going off limits
                if ( leftValue < minLeft) {
                    leftValue = minLeft;
                } else if (leftValue > maxLeft) {
                    leftValue = maxLeft;
                }

                // Translate the handle's left value to masked divs width.
                widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';

                // Set the new values for the slider and the handle.
                // Bind mouseup events to stop dragging.
                $('.draggable').css('left', widthValue).on('mouseup touchend touchcancel', function () {
                    $(this).removeClass('draggable');
                    resizeElement.removeClass('resizable');
                });
                $('.resizable').css('width', widthValue);
            }).on('mouseup touchend touchcancel', function(){
                dragElement.removeClass('draggable');
                resizeElement.removeClass('resizable');
            });
            e.preventDefault();
        }).on('mouseup touchend touchcancel', function(e){
            dragElement.removeClass('draggable');
            resizeElement.removeClass('resizable');
        });
    }
})

/**
 * Menu toggle
 */

$(document).ready(function(){
    $('.nav__burger').on('click', function(){
        $('.header__dropdown-menu').addClass('header__dropdown_active')
        $('.nav__close').addClass('nav__close_active')
        $('.nav__burger').css('display', 'none')
        $('.header__socials-icon').addClass('header__socials_active')
    })
})

$(document).ready(function(){
    $('.nav__close').on('click', function(){
        $('.header__dropdown-menu').removeClass('header__dropdown_active')
        $('.nav__close').removeClass('nav__close_active')
        $('.nav__burger').css('display', 'block')
        $('.header__socials-icon').removeClass('header__socials_active')
    })
})


/**
 * Modal window
 */


const openModalButtons = document.querySelectorAll('[data-modal-target]')
const closeModalButtons = document.querySelectorAll('[data-close-button]')
const overlay = document.getElementById('overlay');

openModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = document.querySelector(button.dataset.modalTarget)
        openModal(modal)
    })
})

closeModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = button.closest('.modal'); //select parent element html
        closeModal(modal)
    })
})

overlay.addEventListener('click', () => {
    const modals = document.querySelectorAll('.modal.active')
    modals.forEach(modal => {
        closeModal(modal)
    })
})

function openModal(modal){
    // if(modal == null){
    //     return
    // }
    if(modal == null) return
    modal.classList.add('active')
    overlay.classList.add('active')
}

function closeModal(modal){
    // if(modal == null){
    //     return
    // }
    if(modal == null) return
    modal.classList.remove('active')
    overlay.classList.remove('active')
}


/**
 * Product modal window
 */


const openModalButtonsProduct = document.querySelectorAll('[data-modal-target-product]')
const closeModalButtonsProduct = document.querySelectorAll('[data-close-button-product]')
const overlayProduct = document.getElementById('overlay-product');

openModalButtonsProduct.forEach(button => {
    button.addEventListener('click', (e) => {
        const modal = document.querySelector(button.dataset.modalTargetProduct)
        openModalProduct(modal)
    })
})

closeModalButtonsProduct.forEach(button => {
    button.addEventListener('click', () => {
        const modal = button.closest('.modal-product'); //select parent element html
        closeModalProduct(modal)
    })
})

overlayProduct.addEventListener('click', (e) => {
    const modals = document.querySelectorAll('.modal-product.active')
    modals.forEach(modal => {
        closeModalProduct(modal)
    })
})

function openModalProduct(modal){
    // if(modal == null){
    //     return
    // }
    if(modal == null) return
    modal.classList.add('active')
    overlayProduct.classList.add('active')
}

function closeModalProduct(modal){
    // if(modal == null){
    //     return
    // }

    if(modal == null) return
    modal.classList.remove('active')
    overlayProduct.classList.remove('active')
}


/**
 * Multi Step Feedback Form
 */


var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
    console.log(currentTab)
    // This function will display the specified tab of the form ...
    var x = document.getElementsByClassName("feedback-tab");
    x[n].style.display = "block";
    // ... and fix the Previous/Next buttons:
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "none";
    }
    if (n == (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Submit";
    } else {
        document.getElementById("nextBtn").innerHTML = "Далее";
    }
    // ... and run a function that displays the correct step indicator:
    fixStepIndicator(n)
}

function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("feedback-tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form... :
    if (currentTab >= x.length) {
        //...the form gets submitted:
        document.getElementById("regForm").submit();
        return false;
    }

    document.querySelector('.counter-current').textContent = currentTab + 1
    // Otherwise, display the correct tab:
    showTab(currentTab);


}

function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("feedback-tab");
    y = x[currentTab].getElementsByClassName("feedback-input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
        // If a field is empty...
        if (y[i].value == "") {
            // add an "invalid" class to the field:
            y[i].className += " invalid";
            // and set the current valid status to false:
            valid = false;
        }
    }

    console.log(x, y)
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
        // document.getElementsByClassName("feedback-step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
}

function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("feedback-step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step:
    // x[n].className += " active";
}

document.querySelector('.counter-total').textContent = document.querySelectorAll('.feedback-tab').length



/**
 * Form validation
 */

const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const tel = document.getElementById('tel');

form.addEventListener('submit', e => {
    e.preventDefault();

    checkInputs();
});

function checkInputs() {
    // trim to remove the whitespaces
    const usernameValue = username.value.trim();
    const emailValue = email.value.trim();
    const telValue = tel.value.trim();

    if(usernameValue === '') {
        setErrorFor(username, 'Username cannot be blank');
    } else {
        setSuccessFor(username);
    }

    if(emailValue === '') {
        setErrorFor(email, 'Incorrect E-mail or Passoword');
    } else if (!isEmail(emailValue)) {
        setErrorFor(email, 'Not a valid email');
    } else {
        setSuccessFor(email);
    }

    if(telValue === '') {
        setErrorFor(tel, 'Incorrect Telephone or Passoword');
    } else if (!isTel(telValue)) {
        setErrorFor(tel, 'Not a valid email');
    } else {
        setSuccessFor(tel);
    }

}

function setErrorFor(input, message) {
    const formControl = input.parentElement;
    const small = formControl.querySelector('small');
    formControl.className = 'form-control error';
    // small.innerText = message;
}

function setSuccessFor(input) {
    const formControl = input.parentElement;
    formControl.className = 'form-control success';
}

function isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}


function isTel(tel) {
    return /^[\+0-9]{11,15}$/.test(tel);
}

/**
 * Phone Mask
 */

$("#tel").mask("+7(999)999-99-99");


/**
 * Scroll Animation desktop block
 */

// init controller
// var controller = new ScrollMagic.Controller();
//
// // create a scene
// new ScrollMagic.Scene({
//     duration: 100, // the scene should last for a scroll distance of 100px
//     offset: 50 // start this scene after scrolling for 50px
// })
//     .setPin('.advice') // pins the element for the the scene's duration
//     .addTo(controller); // assign the scene to the controller

